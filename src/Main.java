package src;


import java.util.Scanner;
import static src.Levels.Level;

public class Main {


    // Function to display the main menu
    public static void main(String[] args)  {
        boolean exit = false;
        Scanner scanner = new Scanner(System.in);

        while (!exit) {
            System.out.println(" **** Main Menu ****");
            System.out.println("1. New Game");
            System.out.println("2. Load Game");
            System.out.println("3. Exit");
            System.out.print("Enter your choice: ");

            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1 -> startNewGame();
                case 2 -> loadGame();
                case 3 -> exit = true;
                default -> System.out.println("Invalid choice. Please try again.");
            }
        }

        scanner.close();
        System.out.println("Exiting the game. Goodbye!");
    }



    // This function works after choosing a new game
    private static void startNewGame()  {
        System.out.println("Starting a new game. Select your desired level.");

        boolean mainMenu = false;
        Scanner scanner = new Scanner(System.in);

        // Level Menu Loop to choose the desired level
        while (!mainMenu) {
            System.out.println(" **** New Game ****");
            System.out.println("1. Beginner 1");
            System.out.println("2. Beginner 2");
            System.out.println("3. Intermediate 1");
            System.out.println("4. Intermediate 2");
            System.out.println("5. Advanced 1");
            System.out.println("6. Advanced 2");
            System.out.println("7. Return to Main Menu");
            System.out.print("Choose your desired level: ");

            int choice = scanner.nextInt();
            scanner.nextLine();


            // Output of the desired level
            switch (choice) {
                case 1 -> {
                    System.out.println("Loading Level Beginner 1");
                    Level beginner1Positions = new Level(2, 3, 3, 2, 5, 1, 2, 1, 4, 4, 4, 5);
                    playGame(beginner1Positions);


                }
                case 2 -> {
                    System.out.println("Loading Level Beginner 2");
                    Level beginner2Level = new Level(2, 3, 3, 2, 5, 1, 2, 1, 9, 9, 4, 5);
                    playGame(beginner2Level);
                }
                case 3 -> {
                    System.out.println("Loading Level Intermediate 1");
                    Level Intermediate1Level = new Level(2, 3, 3, 2, 5, 1, 2, 1, 9, 9, 4, 5);
                    playGame(Intermediate1Level);
                }
                case 4 -> {
                    System.out.println("Loading Level Intermediate 2");
                    Level Intermediate2Level = new Level(2, 3, 3, 2, 5, 1, 2, 1, 9, 9, 4, 5);
                    playGame(Intermediate2Level);

                }
                case 5 -> {
                    System.out.println("Loading Level Advanced 1");
                    Level Advanced1Level = new Level(4, 4, 5, 2, 1, 2, 1, 5, 9, 9,5,5);
                    playGame(Advanced1Level);

                }
                case 6 -> {
                    System.out.println("Loading Level Advanced 2");
                    Level Advanced2Level = new Level(4, 4, 5, 2, 1, 2, 1, 5, 10, 10,5,5);
                    playGame(Advanced2Level);

                }
                case 7 -> mainMenu = true;
                default ->
                        System.out.println("Invalid choice. Please choose the level by digit or return to the Main Menu.");
            }
        }
    }



    private static void loadGame() {
        System.out.println("Loading a saved game...");
        // will Implement game loading logic here
    }



    // Board
    private static final char[][] board = {
            {'*', '*', '*', '*', '*', '*', '*'},
            {'*', ' ', ' ', ' ', ' ', ' ', '*'},
            {'*', ' ', ' ', ' ', ' ', ' ', '*'},
            {'*', ' ', ' ', ' ', ' ', ' ', '*'},
            {'*', ' ', ' ', ' ', ' ', ' ', '*'},
            {'*', ' ', ' ', ' ', ' ', ' ', '*'},
            {'*', '*', '*', '*', '*', '*', '*'}
    };







    // The movement function, still in works
    private static void playGame(Level level)  {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please Select Which Character you want to move");

        while (true) {
            drawBoard(level);

            char input = scanner.nextLine().charAt(0);

            switch (input) {
                case 'w' -> movePlayer(-1, 0);
                case 's' -> movePlayer(1, 0);
                case 'a' -> movePlayer(0, -1);
                case 'd' -> movePlayer(0, 1);
                case 'q' -> System.exit(0);
                default -> System.out.println("Invalid input. Please try again.");
            }
        }
    }

    private static void drawBoard(Level level) {
        char[][] currentBoard = new char[board.length][board[0].length];

        // Copy the main board to the current board
        for (int i = 0; i < board.length; i++) {
            System.arraycopy(board[i], 0, currentBoard[i], 0, board[0].length);
        }

        // Set bot characters on the current board
        currentBoard[level.blueBotX][level.blueBotY] = 'b';
        currentBoard[level.greenBotX][level.greenBotY] = 'g';
        currentBoard[level.orangeBotX][level.orangeBotY] = 'o';
        currentBoard[level.yellowBotX][level.yellowBotY] = 'y';
        currentBoard[level.purpleBotX][level.purpleBotY] = 'p';

        // Set player character on the current board


        // Draw the current board
        for (char[] row : currentBoard) {
            for (char cell : row) {
                System.out.print(cell);
            }
            System.out.println();
        }
    }

    // Bots Letters
    static char[] lettersArray = {'b', 'g', 'o', 'y', 'p'};

    // method to move the player + bots and to check for borders
    public static void movePlayer(int px, int py) {

        Scanner scanner = new Scanner(System.in);

        // Had this problem Non-static field 'motherShipY' cannot be referenced from a static context so i made it static
        int targetX = Level.motherShipX + px;
        int targetY =  Level.motherShipY+ py;


        // Nested Loop to make sure everything works
        if (targetX >= 0 && targetX < board.length && targetY >= 0 && targetY < board[0].length) {
            char targetLetter = board[targetX][targetY];


            // Check if the target letter is in the lettersArray
            boolean isBlocked = false;
            for (char letter : lettersArray) {
                if (letter == targetLetter) {
                    isBlocked = true;
                    break;
                }
            }

            if (board[targetX][targetY] != '*' && !isBlocked) {
                board[Level.motherShipX][Level.motherShipY] = ' ';
                Level.motherShipX = targetX;
                Level.motherShipY = targetY;
                board[Level.motherShipX][Level.motherShipY] = 'M';
            } else {
                System.out.println("Invalid move. Letter in front or obstacle in the way.");
            }
        } else {
            System.out.println("Invalid move. Out of bounds.");
        }
    }}




