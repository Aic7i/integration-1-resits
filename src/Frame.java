package src;

import java.util.ArrayList;
import java.io.IOException;

public class Frame {
    // Game board representation
    private static char[][] board = {
            {'|', '1', '2', '3', '4', '5', '|'},
            {'|', ' ', ' ', ' ', ' ', ' ', '|'},
            {'|', ' ', ' ', ' ', ' ', ' ', '|'},
            {'|', ' ', ' ', ' ', ' ', ' ', '|'},
            {'|', ' ', ' ', ' ', ' ', ' ', '|'},
            {'|', ' ', ' ', ' ', ' ', ' ', '|'},
            {'|', '1', '2', '3', '4', '5', '|'}

    };

    private static int playerX = 4;
    private static int playerY = 4;

    // Function to draw the game board
    private static void drawBoard() {
        for (char[] row : board) {
            for (char cell : row) {
                System.out.print(cell);
            }
            System.out.println();
        }
    }


    // Function to handle user input and move the letters
    private static void handleInput() throws IOException {
        char input = (char) System.in.read();
        switch (input) {
            case 'w':
                if (playerX > 0 && board[playerX - 1][playerY] == ' ') {
                    // Invalid move
                    System.out.println("Invalid move. No letter in front.");
                } else if (playerX > 0 && board[playerX - 1][playerY] != 'd' ) {
                    // Move up
                    board[playerX][playerY] = ' ';
                    playerX--;
                    board[playerX][playerY] = 'A';
                }
                break;
            case 's':
                if (board[playerX + 1][playerY] != '*' && board[playerX + 1][playerY] != 'A') {
                    // Move down
                    board[playerX][playerY] = ' ';
                    playerX++;
                    board[playerX][playerY] = 'A';
                }
                break;
            case 'a':
                if (board[playerX][playerY - 1] != '*' && board[playerX][playerY - 1] != 'A') {
                    // Move left
                    board[playerX][playerY] = ' ';
                    playerY--;
                    board[playerX][playerY] = 'A';
                }
                break;
            case 'd':
                if (board[playerX][playerY + 1] != '*' && board[playerX][playerY + 1] != 'A') {
                    // Move right
                    board[playerX][playerY] = ' ';
                    playerY++;
                    board[playerX][playerY] = 'A';
                }
                break;
            case 'q':
                // Quit the game
                System.exit(0);
        }
    }


    public static void main(String[] args) throws IOException {
        // Main game loop
        while (true) {
            drawBoard();
            handleInput();
        }
    }
}


