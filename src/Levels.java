package src;

public class Levels {


    public static class Level {
         int blueBotX;
         int blueBotY;
         int greenBotX;
         int greenBotY;
         int orangeBotX;
         int orangeBotY;
         int yellowBotX;
         int yellowBotY;
         static int motherShipX;
         static int motherShipY;
         int purpleBotX;
         int purpleBotY;

        public Level(int blueBotX, int blueBotY, int greenBotX, int greenBotY,
                     int orangeBotX, int orangeBotY,int yellowBotX, int yellowBotY, int purpleBotY, int purpleBotX,
                     int motherShipX, int motherShipY) {
            this.blueBotX = blueBotX;
            this.blueBotY = blueBotY;
            this.greenBotX = greenBotX;
            this.greenBotY = greenBotY;
            this.orangeBotX = orangeBotX;
            this.orangeBotY = orangeBotY;
            this.yellowBotX = yellowBotX;
            this.yellowBotY = yellowBotY;
            this.purpleBotX = purpleBotX;
            this.purpleBotY = purpleBotY;
            this.motherShipX = motherShipX;
            this.motherShipY = motherShipY;
        }


    }


}

